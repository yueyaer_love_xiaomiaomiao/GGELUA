--[[
    @Author       : GGELUA
    @Date         : 2021-04-24 16:36:37
    @LastEditTime : 2021-05-08 01:50:47
--]]
local im = require"gimgui"
local IMBase = require"IMGUI.基类"

local IM按钮 = class('IM按钮',"IMBase")

function IM按钮:初始化(name,def)
    self._name = name
    self[1] = def==true
end

function IM按钮:显示(x,y)
    return im.Button(self._name)
end
--=====================================================
local IM单选按钮 = class('IM单选按钮',"IMBase")
package.loaded["IMGUI.单选按钮"] = IM单选按钮

function IM单选按钮:初始化(name,def)
    self._name = name
    self[1] = def==true
end

function IM单选按钮:显示(x,y)
    return im.RadioButton(self._name,self)
end
--=====================================================
local IM复选按钮 = class('IM复选按钮',"IMBase")
package.loaded["IMGUI.复选按钮"] = IM复选按钮

function IM复选按钮:初始化(name,def)
    self._name = name
    self[1] = def==true
end

function IM复选按钮:显示(x,y)
    return im.Checkbox(self._name,self)
end

--=====================================================
local IM纹理按钮 = class('IM纹理按钮',"IMBase")
package.loaded["IMGUI.纹理按钮"] = IM纹理按钮

function IM纹理按钮:初始化(tex,def)
    self._tex = tex
end

function IM纹理按钮:显示(x,y)
    local ptr = self._tex:取对象():GetTexturePointer()
    return im.ImageButton(ptr)
end

return IM按钮