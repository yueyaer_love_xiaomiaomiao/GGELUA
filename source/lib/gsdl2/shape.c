#include "gge.h"
#include "SDL_shape.h"

static int LUA_CreateShapedWindow(lua_State* L)
{
    const char* name = luaL_checkstring(L, 1);
    int x = (int)luaL_optinteger(L, 2, SDL_WINDOWPOS_UNDEFINED);
    int y = (int)luaL_optinteger(L, 3, SDL_WINDOWPOS_UNDEFINED);
    int width = (int)luaL_checkinteger(L, 4);
    int height = (int)luaL_checkinteger(L, 5);
    int flags = (int)luaL_checkinteger(L, 6);
    SDL_Window* win = SDL_CreateShapedWindow(name, x, y, width, height, flags);
    if (win) {
        SDL_Window** ud = (SDL_Window**)lua_newuserdata(L, sizeof(SDL_Window*));
        *ud = win;
        lua_pushinteger(L, 1);
        lua_setuservalue(L, -2);
        luaL_setmetatable(L, "SDL_Window");
        return 1;
    }
    return 0;
}

static int LUA_IsShapedWindow(lua_State* L)
{
    SDL_Window* win = *(SDL_Window**)luaL_checkudata(L, 1, "SDL_Window");

    lua_pushboolean(L, SDL_IsShapedWindow(win) == SDL_TRUE);
    return 1;
}

static int LUA_SetWindowShape(lua_State* L)
{
    SDL_WindowShapeMode shape_mode;
    SDL_Window* win = *(SDL_Window**)luaL_checkudata(L, 1, "SDL_Window");
    SDL_Surface* sf = *(SDL_Surface**)luaL_checkudata(L, 2, "SDL_Surface");
    shape_mode.mode = (WindowShapeMode)luaL_checkinteger(L, 3);
    shape_mode.parameters.binarizationCutoff = (Uint8)luaL_checkinteger(L, 4);
    //shape_mode.parameters.colorKey = toSDL_Color(luaL_checkinteger(L,5));
    lua_pushinteger(L, SDL_SetWindowShape(win, sf, &shape_mode));
    return 1;
}

static int LUA_GetShapedWindowMod(lua_State* L)
{
    SDL_Window* win = *(SDL_Window**)luaL_checkudata(L, 1, "SDL_Window");
    SDL_WindowShapeMode shape_mode;
    if (SDL_GetShapedWindowMode(win, &shape_mode) == 0)
    {
        lua_pushinteger(L, shape_mode.mode);
        lua_pushinteger(L, shape_mode.parameters.binarizationCutoff);
        lua_pushinteger(L, *(int*)&shape_mode.parameters.colorKey);
        return 3;
    }
    return 0;
}

static const luaL_Reg window_funcs[] = {
    {"IsShapedWindow"      , LUA_IsShapedWindow},
    {"SetWindowShape"      , LUA_SetWindowShape},
    {"GetShapedWindowMod"      , LUA_GetShapedWindowMod},
    { NULL, NULL}
};

static const luaL_Reg sdl_funcs[] = {
    {"CreateShapedWindow"      , LUA_CreateShapedWindow},
    { NULL, NULL}
};

int bind_shape(lua_State* L)
{
    luaL_getmetatable(L, "SDL_Window");
    luaL_setfuncs(L, window_funcs, 0);
    lua_pop(L, 1);

    luaL_setfuncs(L, sdl_funcs, 0);
    return 0;
}